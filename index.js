numList = []

function themSo(){
    var num = document.getElementById("number").value * 1;
    
    numList.push(num);
    document.getElementById("result__1").innerHTML = `${numList}`
    document.getElementById("result__1").style.display = "inline-block";
}

function tongSoDuong(){
    var tongSo = 0;
    for(var i=0;i<numList.length;i++){
        if(numList[i] > 0){
            tongSo += numList[i]
        }
    }
    document.getElementById("result__2").innerHTML = `Tổng các số dương: ${tongSo}`;
    document.getElementById("result__2").style.display = "inline-block";
}

function demSoDuong(){
    var soDuong = 0;
    for(var i =0;i<numList.length;i++){
        if(numList[i]>0){
            soDuong += 1;
        }
    }
    document.getElementById("result__3").innerHTML = `Tổng ${soDuong} số dương`;
    document.getElementById("result__3").style.display = "inline-block";
}

function minimum(input) {
    var min =input[0];
    for(var i=1;i<input.length;i++){
        if(input[i] < min){
            min=input[i];
        }
    }
    return min
}
function soNhoNhat(){
    document.getElementById("result__4").innerHTML = `Số nhỏ nhất: ${minimum(numList)}`;
    document.getElementById("result__4").style.display = "inline-block";
}

function soDuongNhoNhat(){
    var soDuongList = []
    for(var i=0;i<numList.length;i++){
        if (numList[i]>0) {
            soDuongList.push(numList[i]);
        }
    }
    if(soDuongList.length ==0){
        document.getElementById("result__5").innerHTML = `Không có số dương nào`;
    }else
    {
    document.getElementById("result__5").innerHTML = `Số dương nhỏ nhất: ${minimum(soDuongList)}`;
    }
    document.getElementById("result__5").style.display = "inline-block";
}

function soChanCuoiCung(){
    var soChan = []
    for(var i=0;i<numList.length;i++){
        if(numList[i]%2 == 0&&numList[i]!=0){
            soChan.push(numList[i]);
        }
    }
    if(soChan.length == 0){
        document.getElementById("result__6").innerHTML = `Không có số chẵn nào`;
    }else{
        document.getElementById("result__6").innerHTML = `Số chẵn cuối cùng: ${soChan[soChan.length-1]}`;
    }
    document.getElementById("result__6").style.display = "inline-block";    
}

function doiCho() {
    var temp = numList.slice();
    var x = document.getElementById("X").value *1;
    var y = document.getElementById("Y").value *1;
    if(Number.isInteger(x)==false || Number.isInteger(y==false) || x<0 || y<0){
        alert("Du lieu khong hop le");
    }
    [temp[x], temp[y]] = [temp[y], temp[x]];
    document.getElementById("result__7").innerHTML = `Dữ liệu đã thay đổi: ${temp}`;
    document.getElementById("result__7").style.display = "inline-block"; 
}

function sort(input){
    var min;
    for (var i = 0; i < input.length; i++) {
        min = i;
        for (var j = i + 1; j < input.length; j++) {
            if (input[j] < input[min]) {
                 min = j;
            }
        }
        if (min !== i) {
            [input[i], input[min]] = [input[min], input[i]];
            }
        }
        return input;
}
function sapXepTangDan(){
    temp = numList.slice();
    
    document.getElementById("result__8").innerHTML = `Dữ liệu sau khi sắp xếp:${sort(temp)}`;
    document.getElementById("result__8").style.display = "inline-block"; 
}

function timSoNguyenTo(){
    function primer(input){
        var isPrime = true;
        if(input<=1){
            return false
        }else{
            for (var i = 2; i<input; i++){
                if (input % i == 0){
                    isPrime = false;
                    break;
                    }
                }
            if(isPrime == true){return true}
            else{return false}
            }
        }
    for(var i=0;i<numList.length;i++){
        if(primer(numList[i]) == true){
            document.getElementById("result__9").innerHTML = `Số nguyên tố đầu tiên là: ${numList[i]}`;
            break;
        }
    }
    
    document.getElementById("result__9").style.display = "inline-block"; 
}

numList_2 = []
function themSo_2(){
    var num_2 = document.getElementById("number__2").value * 1;
    
    numList_2.push(num_2);
    document.getElementById("result__10").innerHTML = `${numList_2}`
    document.getElementById("result__10").style.display = "inline-block";
}

function demSoNguyen(){
    var count = 0;
    for (var i=0;i<numList_2.length;i++){
        if(Number.isInteger(numList_2[i])==true){
            count ++;
        }
    }
    document.getElementById("result__11").innerHTML = `Số Nguyên: ${count}`
    document.getElementById("result__11").style.display = "block";
}

function soSanhAmDuong(){
    var soAm = 0;
    var soDuong = 0;
    for(var i=0;i<numList.length;i++){
        if(numList[i]<0){
            soAm ++;
        }else if(numList[i]>0){
            soDuong ++;
        }
    }
    if(soAm>soDuong){
        document.getElementById("result__12").innerHTML = `Số Âm > Số Dương`;
    }else if(soAm<soDuong){
        document.getElementById("result__12").innerHTML = `Số Âm < Số Dương`
    }
    document.getElementById("result__12").style.display = "inline-block";
}